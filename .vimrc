filetype off 
call pathogen#runtime_append_all_bundles()

filetype plugin indent on
syntax on

set nocompatible

set modelines=0

set hidden

set wildmenu

set showcmd

set hlsearch


" Color scheme (terminal)
syntax on
set background=dark
colorscheme desert
if has('gui_running')
    colorscheme molokai
    set guifont=consolas:h11:b
    set background=dark

    set go-=T
    set go-=l
    set go-=L
    set go-=r
    set go-=R

    let g:sparkupExecuteMapping = '<D-e>'

    highlight SpellBad term=underline gui=undercurl guisp=Orange
endif


"------------------------------------------------------------

" Usability options 

"

set backspace=indent,eol,start

set autoindent

set smartindent

set nostartofline

set ruler

set laststatus=2

set confirm

set visualbell

set t_vb=

set mouse=a

set cmdheight=2

set number

set ruler

set notimeout ttimeout ttimeoutlen=200

set pastetoggle=<F11>

set guioptions-=T



set encoding=utf-8

set scrolloff=3

set showmode

set wildmode=list:longest

set visualbell

set cursorline

set ttyfast

let mapleader = ","

set wrap

set textwidth=79

set formatoptions=qrn1

" NERD Tree
map <F2> :NERDTreeToggle<cr>
let NERDTreeIgnore=['.vim$', '\~$', '.*\.pyc$', 'pip-log\.txt$']


" Exuberant ctags!
let Tlist_Ctags_Cmd = "/usr/local/bin/ctags"
let Tlist_WinWidth = 50
map <F4> :TlistToggle<cr>
map <F5> :!/usr/local/bin/ctags -R --c++-kinds=+p --fields=+iaS --extra=+q --exclude='@.ctagsignore' .<cr>"custom hotkeys


" Use the damn hjkl keys
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>

" And make them fucking work, too.
nnoremap j gj
nnoremap k gk
" Faster Esc
inoremap jj <ESC>

" Easy buffer navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
map <leader>w <C-w>v<C-w>l

nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>

nnoremap <leader>ft Vatzf

nnoremap <leader>w <C-w>v<C-w>l

nnoremap <C-h> <C-w>h

nnoremap <C-j> <C-w>j

nnoremap <C-k> <C-w>k

nnoremap <C-l> <C-w>l









"Searching 



nnoremap / /\v

vnoremap / /\v

set ignorecase

set smartcase

set gdefault

set incsearch

set showmatch

set hlsearch

nnoremap <leader><space> :noh<cr>

nnoremap <tab> %

vnoremap <tab> %





"------------------------------------------------------------

" Indentation options 

"

set tabstop=4

set shiftwidth=4

set softtabstop=4

set expandtab
